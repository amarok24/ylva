# Ylva - Command line password manager

Current version: 1.7

* This is a fork from https://github.com/nrosvall/ylva

Password management belongs to the command line, deep into the Unix heartland,
the shell.

Ylva is written in C and is licensed under the MIT license. Ylva is mostly developed
on Linux, but should work on BSD too.
